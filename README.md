# GIT #

All GIT commands in one page. Simeon Stavrev
***
### Bare repository ###
* cd /opt/git
* mkdir myproject
* cd project
* git --bare init
***
### Init set up of project ###
* cd myproject
* git init
* git add .
* git commit -m 'Initial commit'
* git remote add origin /opt/git/myproject
* git push origin master
***
### Commit ###
* git status - list of staged files
* git add . - stage for commit
* git commit -m "Message" - commit files
* git tree - list of short commits (alias)
* git ac -m "message" - add and commit (alias)
* git checkout filename - file reset
***
### Push and pull ###
* git pull origin master - pull request from repository _origin_ and bransh _master_
* git push origin master - push request to repository _origin_ and bransh _master_
***
### Branches ###
* git branch - current branch + all branches
* git branch newbranch - creating new branch _newbranch_
* git checkout newbranch - _newbranch_ becomes active branch
* git merge newbranch - merges _newbranch_ to the current branch
* git merge --squash newbranch - combine multiple commits into one
* git merge --no-commit branchname
***
### Show and diff ###
* git diff - shows all changes from last commit
* git diff name-of-file.php - shows changwes in one file
* git log - shows log
* git show commitHash - shows commit changes
* git show --pretty="format:" --name-only commitHash
* git show commitHash:/path/to/file - shows file from commit
***
### git-ftp ###
https://github.com/git-ftp/git-ftp

* apt-get install git-ftp - installation
* git config git-ftp.url "ftp://ftp.example.net:21/public_html"
* git config git-ftp.user "ftp-user"
* git ftp catchup - if the files are already on FTP
* git ftp push -P - before or after a git push - push with password promt